import { React } from 'react';
import './App.css';
import { FilterTable } from '../component/FilterableMovieTable';
import { BrowserRouter } from 'react-router-dom';

const App = () => {

  return (
    <div className='app'>
      <BrowserRouter>
        <FilterTable />
      </BrowserRouter>
    </div>
  )
}




export default App;

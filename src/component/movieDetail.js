import './styles.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams, useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';

const base = 'https://api.themoviedb.org/3/movie/';
const apiKey = '?api_key=a02f1a6f22f1c51fd8e9b7de92a83bb5';

const MovieDetail = () => {
    const location = useLocation();
    const idArray = location.state;

    const [array, setArray] = useState(idArray);
    const { id } = useParams();

    const [movie, setMovie] = useState();
    const movieUrl = `${base}${id}${apiKey}`;

    useEffect(() => {
        axios.get(movieUrl)
            .then(res => {
                setMovie(res.data);
                setArray(idArray);
            })
            .catch(err => console.error(err))
    });

    let prevInd = (array.indexOf(parseInt(id)) === 0) ? -1 : (array.indexOf(parseInt(id))) - 1;
    let nextInd = (array.indexOf(parseInt(id)) === array.length - 1) ? -1 : (array.indexOf(parseInt(id))) + 1;
    let prev = prevInd === -1 ? -1 : array[prevInd];
    let next = nextInd === -1 ? -1 : array[nextInd];
    let prevClass = prevInd === -1 ? -1 : '';
    let nextClass = nextInd === -1 ? -1 : '';

    return (
        <>
            <div className="card">
                <div className="card-body">
                    {movie
                        ? (
                            <>
                                <Link className={`prev${prevClass}`} to={`/gallery/${prev}`} state={array} > Prev </Link>
                                <div className='card-wrapper'>
                                    <img src={'https://image.tmdb.org/t/p/original/' + movie.poster_path} alt="moviePoster" ></img>
                                </div>
                                <div className='card-text-wrapper'>
                                    <h3>Overview</h3>
                                    <div className='card-text'>
                                        <div>
                                            <div>
                                                <span>Name: {movie.original_title}</span>
                                                <br />
                                                <span>Rate: {movie.vote_average.toFixed(1)}</span>
                                            </div>
                                            <span>Genre: </span>
                                            {movie.genres.map(
                                                genre => <span key={genre.id}>
                                                    {genre.name} &nbsp;
                                                </span>
                                            )}
                                            <div><br />
                                                {movie.overview}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Link className={`next${nextClass}`} to={`/gallery/${next}`} state={array} > Next </Link>
                            </>
                        )
                        : <>There is an error with this movie, please check out other movies</>
                    }
                </div>
            </div>
        </>
    );
};

export default MovieDetail;
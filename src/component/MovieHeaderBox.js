import GenreFilter from "./gallery/MovieGenreFilter";
import MovieSearchBox from "./list/MovieSearchBox";
import { Routes, Route, Link } from "react-router-dom";
import MovieDetail from "./movieDetail";

const HeaderBox = () => {
    return (
        <>
            <div className="text-center">
                <h1 className="display-4 text-center mb-3">TMDB Movie Search</h1>
                <div className="btn-wrapper">
                    <button className="btn btn-primary my-3 btn-gallery"><Link to="/gallery/">Gallery</Link></button>
                    <button className="btn btn-primary my-3 btn-list"><Link to="/list/">List</Link></button>
                </div>
            </div>
        </>
    )
};

const MovieHeaderBox = () => {

    return (
        <div className="container">
            <HeaderBox />
            <Routes>
                <Route path='/' element={<> </>} />
                <Route path='/gallery' element={<GenreFilter />}/>
                <Route path='/gallery/:id' element={<MovieDetail  /> }/>
                <Route path='/list/' element={ <MovieSearchBox />}/>
            </Routes> 
            <br />
        </div>
    )
};

export default MovieHeaderBox;
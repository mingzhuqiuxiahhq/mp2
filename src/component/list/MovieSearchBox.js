import { useState,  } from 'react';
import MovieGallery from '../MovieGalleryDisplay';
import * as api from '../../Api/TMDB';
import axios from 'axios';
import { Radio } from '@mui/material';

const search = api.search;

const MovieSearchBar = ({searchItem}) => {
    return (
        <div className='MovieSearchBar'>
            <div className='input-group mb-3'>
                <div className="input-group-prepend">
                    <i className="fa fa-search"></i>
                    <input type="text" onChange={(e) => searchItem(e.target.value)} className="form-control searchBar" placeholder='Enter any keyword...' />
                </div>
            </div>
        </div>
    )
};


const MovieSearchBox = () => {

    const [movies, setMovies] = useState([]);
    const [input, setInput] = useState('');
    const qu = `&query=`;


    const searchItem = (inputVal) => {
        setInput(inputVal);
        let searchUrl = `${search}${qu}${input}`;

        if (inputVal !== '' & inputVal !== ' ') {
            axios.get(searchUrl)
                .then(res => {
                    setMovies(res.data.results);
                    console.log(res.data.results);
                })
                .catch(err => { console.error(err) });
        } else {

        }
    }

    const [isToggled, setToggle] = useState('Year Desc');
    const handleToggle = (event) => {

        const sortMethod = event.target.value;
        setToggle(event.target.value);;

        const newMovies = movies;
        if (sortMethod === 'Year Desc') {

            newMovies.sort((a, b) => {
                var dateA = new Date(a.release_date);
                var dateB = new Date(b.release_date);
                return dateA - dateB;
            }).reverse();

        } else if (sortMethod === 'Year Asc') {
            newMovies.sort((a, b) => {
                var dateA = new Date(a.release_date);
                var dateB = new Date(b.release_date);
                return dateA - dateB;
            });

        }else if (sortMethod === 'Popularity Desc') {
            newMovies.sort((a, b) =>
                a.popularity - b.popularity
            ).reverse();
        }else if (sortMethod === 'Popularity Asc') {
            newMovies.sort((a, b) =>
                a.popularity - b.popularity
            );
        }
        setMovies(newMovies);
    };

    return (
        <>
            {/* <div className='MovieSearchBar'>
                <div className='input-group mb-3'>
                    <div className="input-group-prepend">
                        <i className="fa fa-search"></i>
                        <input type="text" onChange={(e) => searchItem(e.target.value)} className="form-control searchBar" placeholder='Enter any keyword...' />
                    </div>
                </div>
            </div> */}
            <MovieSearchBar searchItem={searchItem} />
            <div className="filterSwitch">
                <div className="filterRadio">
                    <Radio checked={isToggled === 'Year Desc'} onClick={handleToggle} value={"Year Desc"} name="radio-btn" inputProps={{ 'aria-label': 'Ascending' }} /> Year Desc
                </div>
                <div className="filterRadio">
                    <Radio checked={isToggled === 'Year Asc'} onClick={handleToggle} value={"Year Asc"} name="radio-btn" inputProps={{ 'aria-label': 'Ascending' }} /> Year Asc
                </div>
                <div className="filterRadio">
                    <Radio checked={isToggled === 'Popularity Desc'} onClick={handleToggle} value={"Popularity Desc"} name="radio-btn" inputProps={{ 'aria-label': 'Descending' }} /> Popularity Desc
                </div>
                <div className="filterRadio">
                    <Radio checked={isToggled === 'Popularity Asc'} onClick={handleToggle} value={"Popularity Asc"} name="radio-btn" inputProps={{ 'aria-label': 'Descending' }} /> Popularity Asc
                </div>
            </div>
            <div className="listContainer">
                <MovieGallery movies={movies} posterSize='poster-sm' />
            </div>
        </>
    );
};

export default MovieSearchBox;
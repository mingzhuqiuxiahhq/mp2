import { React } from 'react';

function MovieSearchBar() {
    return (
        <div className='MovieSearchBar'>
            <div className='input-group mb-3'>
                <div className="input-group-prepend">
                    <i className="fa fa-search"></i>
                    <input type="text" className="form-control searchBar" placeholder='Enter any keyword...'/>
                </div>
            </div>
        </div>
    )
}

export default MovieSearchBar;
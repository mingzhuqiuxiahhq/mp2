import { Component, useState, useEffect } from "react";
import "../styles.css";
import { genres } from "../../constants/genreType";
import PropTypes from "prop-types";
import MovieGallery from "../MovieGalleryDisplay";
import * as api from "../../Api/TMDB.js";
import axios from "axios";
const base = api.base;

Component.PropTypes = {
    clickedGenre: PropTypes.func,
};

const GenreFilter = () => { 
    const [selectedGenre, setGenre] = useState('All');
    const [movies, newMovies] = useState();
    const [url, setUrl] = useState(base);
    const movieArr = [];

    var withGenre = `&with_genres=`;
    var filteredUrl;

    useEffect(() => {

        axios.get(`${url}`)
            .then(res => {
                newMovies(res.data.results);
                idToArr(res.data.results);
            })
            .catch(err => { console.error(err)});
    });

    function fetchMovie (filteredUrl) {
        axios.get(filteredUrl)
            .then(res => {
                newMovies(res.data.results);
            })
            .catch(err => {
                console.error(err)
            });
    };

    function idToArr (array) {
        if(array) { 
            array.forEach(arr => {
                movieArr.push(arr.id);
            });
        } 

        console.log(typeof movieArr);
    };

    // for filter genres
    const clickedGenre = (event) => {
        var eventTargetVal = event.target.value;

        if(eventTargetVal === 'All'){
            fetchMovie(base);
            return;
        }

        var prevGenre = selectedGenre;

        var eventTargetId = event.target.id;

        if (prevGenre !== eventTargetVal) {
            document.querySelector(`.${prevGenre}`).classList.remove("btn-secondary");
            document.querySelector(`.${eventTargetVal}`).classList.add("btn-secondary");
        }

        filteredUrl = `${base}${withGenre}${eventTargetId}`;
        setUrl(`${base}${withGenre}${eventTargetId}`);
        fetchMovie(filteredUrl);
        setGenre(event.target.value);        
    };

    return (
        <>
            <div className="genreFilterBar">
                {genres.map((genre) =>
                    <button className={`btn btn-primary genreBtn active ${genre.name}`} onClick={clickedGenre} value={genre.name} id={genre.id} key={genre.id} >{genre.name}</button>)
                }
            </div>
            <br />
            <div className="movieContainer">
                <MovieGallery movies={movies} posterSize='poster-lg' movieIdArray={movieArr}  />
            </div>
        </>
    );
};

export default GenreFilter;
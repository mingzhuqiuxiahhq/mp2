import "./styles.css";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

const MovieGallery = ({ movies, posterSize, movieIdArray }) => {

    const [IdArray, setIdArray] = useState();
    let mode = posterSize === 'poster-sm' ? 'list' : 'gallery';

    useEffect(() => {
        setIdArray(movieIdArray);
    }, [movieIdArray]);

    return (
        movies !== [] ?
            movies?.map(
                movie =>
                    <div className={'movieGallery'} key={movie.id}>
                        <div className={`moviePoster ${posterSize}`} >
                            { mode === 'gallery' 
                                ? (<Link to={`/gallery/${movie.id}`} state={ IdArray } >
                                    <img src={'https://image.tmdb.org/t/p/original/' + movie?.poster_path} alt="moviePoster" />
                                </Link>)
                                : (<img src={'https://image.tmdb.org/t/p/original/' + movie?.poster_path} alt="moviePoster" />)
                            }
                        </div>
                        <div className={`movieTitle `}>
                            {movie.original_title}
                        </div>
                    </div>
            )
            : <></>
    )
}

export default MovieGallery;